# Bank Application


Ce projet est pour le katas C# Bank Account de Exalt-It.


Le projet est une application web MVC sous .net core 6.0 avec des routes Api, le projet est basé sur l'architecture propre (Clean Architecture).




### Dans Le projet :


- L'architecture propre (Clean architecture), cette architecture consiste à diviser le projet en trois couches:

	- Domain: cette couche est le centre de l'application est elle est indépendante des autres couches, elle contient tout l'aspect métier (classes métier, interfaces métier...).
	- Infrastructure: cette couche est dépendante de la couche Domain, elle contient tous ce qui est infrastructure, comme l'accès à une base de données, accès aux service externes… Généralement elle implémente les interfaces déclarer par la couche métier.
	- Application: cette couche est le point d'entré de l'application, elle  est dépendante de la couche Domain et Infrastructure, elle interagit avec l’infrastructure uniquement par le biais des interfaces définies dans la couche Domain de l’application.

- Page Swagger pour décrire et tester toutes les routes API facilement.


- Le pattern Mediator pour réduire le couplage des classes et services.


- FluentValidation pour la validation des modèles d'entrée.


- La Gestion des erreurs selon l'environnement de Dev ou Prod.


- Injection de dépendance avec Autofac, ce qui permet d'enregistrer des injections de dépendances génériques.


- Base de données : SQLite (La base utilise des fichiers pour stocker les données)


- L'utilisation d'ORM Entity Framework avec le système de migration.


- Tests Unitaires (MSTest) en utilisant le système de Mock.

- Tests d'intégrations 

- Script CI/CD pour build et lancer les tests du projet. 


- Docker pour lancer l'application dans un conteneur.




### Build le projet:


Pour build le projet, il faut installer le SDK .net core 6.0 ou une autre version ultérieure.


Dans la racine du projet, exécuter les commandes :


- `dotnet restore`


- `dotnet build`




### Exécuter le projet


Dans la racine du projet :


- `cd Bank.Application`


- `dotnet run`


Une fois, l'application est lancée, aller sur le lien affiché dans la console.


Normalement, ça doit être: [https://localhost:7183](https://localhost:7183).






L'application va vous rediriger directement sur Swagger.






Ps: pour tester Swagger, un compte est avec ID: 1 , est déjà enregistré dans la base, vous pouvez l'utiliser pour faire des dépôts et des retraits de l'argent dans le compte.

### Docker

Le projet contient un fichier Docker pour contenariser l'application:

#### Commandes:

- créer l'image: `docker build --rm -t bank:latest .`

- créer et demarrer le conteneur: `docker run --rm -p 5000:5000 -p 5001:5001 -e ASPNETCORE_HTTP_PORT=https://+:5001 -e ASPNETCORE_URLS=http://+:5000 bank`

- Aller sur l'Url: http://localhost:5000

