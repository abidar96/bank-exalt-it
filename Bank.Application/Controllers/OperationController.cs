﻿using Bank.Application.Application.Commands;
using Bank.Application.Application.Dtos;
using Bank.Application.Application.Exceptions;
using Bank.Application.Application.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Bank.Application.Controllers;

[Route("api/Operation")]
public class OperationController : ControllerBase
{
    private readonly IMediator _mediator;

    public OperationController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [SwaggerOperation(Summary = "Make a deposit")]
    [Consumes("application/json")]
    [Produces("application/json")]
    [ProducesResponseType(typeof(DepositDto), 200)]
    [HttpPost("Deposit")]
    public async Task<DepositDto> Deposit([FromBody] DepositCommand depositCommand)
    {
        return await _mediator.Send(depositCommand);   
    }

    [SwaggerOperation(Summary = "Make a withdrawal")]
    [Consumes("application/json")]
    [Produces("application/json")]
    [ProducesResponseType(typeof(HistoryDto), 200)]
    [HttpPost("Retrive")]
    public async Task<HistoryDto> Retrive([FromBody] RetrieveCommand retriveCommand)
    {
        return await _mediator.Send(retriveCommand);
    }

    [SwaggerOperation(Summary = "Check history")]
    [Produces("application/json")]
    [ProducesResponseType(typeof(IEnumerable<HistoryDto>), 200)]
    [HttpGet("{accountId}/History")]
    public async Task<IEnumerable<HistoryDto>> History([FromRoute] int accountId)
    {
        return await _mediator.Send(new HistoryQuery() { AccountId = accountId});
    }

}
