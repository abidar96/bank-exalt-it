﻿using Bank.Application.Application.Dtos;
using Bank.Application.Application.Exceptions;
using Bank.Domain.Entities;
using Bank.Domain.Exceptions;
using Bank.Domain.Repositories;
using MediatR;

namespace Bank.Application.Application.Commands;

public class RetrieveCommand : IRequest<HistoryDto>
{
    public int AccountId { get; set; }

    public int Amount { get; set; }
}

public class RetriveCommandHandler : IRequestHandler<RetrieveCommand, HistoryDto>
{
    private readonly IAccountRepository _accountRepository;
    private readonly IOperationRepository _operationtRepository;

    public RetriveCommandHandler(IAccountRepository accountRepository, IOperationRepository operationRepository )
    {
        _accountRepository = accountRepository;
        _operationtRepository = operationRepository;
    }

    public async Task<HistoryDto> Handle(RetrieveCommand request, CancellationToken cancellationToken)
    {
        var account = await _accountRepository.GetByIdAsync(request.AccountId);
        if (account == null)
            throw new NotFoundException($"No account found with this Id: {request.AccountId}");

        if(account.Balance < request.Amount)
            throw new FunctionalException("Your balance is not sufficient to perform this operation");

        var operation = new Operation()
        {
            AccountId = request.AccountId,
            Amount = request.Amount * (-1),
            Balance = account.Balance-request.Amount,
            Date = DateTime.UtcNow
        };
        var addedOperation = await _operationtRepository.AddAsync(operation);

        account.Balance -= request.Amount;
        _accountRepository.Update(account);

        await _operationtRepository.SaveChangesAsync();
        await _accountRepository.SaveChangesAsync();

        return HistoryDto.FromOperation(addedOperation);
    }
}
