﻿using Bank.Application.Application.Dtos;
using Bank.Application.Application.Exceptions;
using Bank.Domain.Entities;
using Bank.Domain.Repositories;
using MediatR;

namespace Bank.Application.Application.Commands;

public class DepositCommand : IRequest<DepositDto>
{
    public int AccountId { get; set; }

    public int Amount { get; set; }

}

public class DepositCommandHandler : IRequestHandler<DepositCommand, DepositDto>
{
    private readonly IAccountRepository _accountRepository;
    private readonly IOperationRepository _operationtRepository;

    public DepositCommandHandler(IAccountRepository accountRepository, IOperationRepository operationRepository )
    {
        _accountRepository = accountRepository;
        _operationtRepository = operationRepository;
    }

    public async Task<DepositDto> Handle(DepositCommand request, CancellationToken cancellationToken)
    {
        var account = await _accountRepository.GetByIdAsync(request.AccountId);
        if (account == null)
            throw new NotFoundException($"No account found with this Id: {request.AccountId}");

        var operation = new Operation()
        {
            AccountId = request.AccountId,
            Amount = request.Amount,
            Balance = account.Balance+request.Amount,
            Date = DateTime.UtcNow
        };
        var addedOperation = await _operationtRepository.AddAsync(operation);

        account.Balance += request.Amount;
        _accountRepository.Update(account);


        await _operationtRepository.SaveChangesAsync();
        await _accountRepository.SaveChangesAsync();

        return DepositDto.FromOperation(addedOperation);
    }
}
