﻿using Bank.Domain.Entities;

namespace Bank.Application.Application.Dtos;

public class DepositDto
{
    public int Id { get; set; }

    public int Amount { get; set; }

    public int Balance { get; set; }

    public DateTime Date { get; set; }

    public static DepositDto FromOperation(Operation operation)
    {
        return new DepositDto()
        {
            Id = operation.Id,
            Amount = operation.Amount,
            Balance = operation.Balance,
            Date = operation.Date
        };
    }
}
