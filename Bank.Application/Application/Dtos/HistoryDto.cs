﻿using Bank.Domain.Entities;

namespace Bank.Application.Application.Dtos;

public class HistoryDto
{
    public int Amount { get; set; }

    public int Balance { get; set; }

    public DateTime Date { get; set; }

    public static HistoryDto FromOperation(Operation operation)
    {
        return new HistoryDto()
        {
            Amount = operation.Amount,
            Balance = operation.Balance,
            Date = operation.Date
        };
    }
}
