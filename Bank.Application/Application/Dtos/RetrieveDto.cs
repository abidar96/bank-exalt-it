﻿using Bank.Domain.Entities;

namespace Bank.Application.Application.Dtos;

public class RetrieveDto
{
    public int Id { get; set; }

    public int Amount { get; set; }

    public int Balance { get; set; }

    public DateTime Date { get; set; }

    public static RetrieveDto FromOperation(Operation operation)
    {
        return new RetrieveDto()
        {
            Id = operation.Id,
            Amount = operation.Amount * (-1),
            Balance = operation.Balance,
            Date = operation.Date
        };
    }
}
