﻿using Bank.Application.Application.Commands;
using FluentValidation;

namespace Bank.Application.Application.Validators;

public class RetriveCommandValidator : AbstractValidator<RetrieveCommand>
{
    public RetriveCommandValidator()
    {
        RuleFor(retrive => retrive.AccountId)
            .NotEmpty()
            .WithMessage("Account Id must not be null");

        RuleFor(deposit => deposit.Amount)
            .NotEmpty()
            .WithMessage("Amount must not be null");
    }
}
