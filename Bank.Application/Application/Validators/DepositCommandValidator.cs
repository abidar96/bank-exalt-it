﻿using Bank.Application.Application.Commands;
using FluentValidation;

namespace Bank.Application.Application.Validators;

public class DepositCommandValidator : AbstractValidator<DepositCommand>
{
    public DepositCommandValidator()
    {
        RuleFor(deposit => deposit.AccountId)
            .NotEmpty()
            .WithMessage("Account Id must not be null");

        RuleFor(deposit => deposit.Amount)
            .NotEmpty()
            .WithMessage("Amount must not be null");
    }
}
