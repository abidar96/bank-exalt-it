﻿using Bank.Application.Application.Dtos;
using Bank.Application.Application.Exceptions;
using Bank.Domain.Repositories;
using MediatR;

namespace Bank.Application.Application.Queries;

public class HistoryQuery : IRequest<IEnumerable<HistoryDto>>
{
    public int AccountId { get; set; }
}

public class HistoryQueryHandler : IRequestHandler<HistoryQuery, IEnumerable<HistoryDto>>
{
    private readonly IAccountRepository _accountRepository;
    private readonly IOperationRepository _operationtRepository;

    public HistoryQueryHandler(IAccountRepository accountRepository, IOperationRepository operationRepository)
    {
        _accountRepository = accountRepository;
        _operationtRepository = operationRepository;
    }

    public async Task<IEnumerable<HistoryDto>> Handle(HistoryQuery request, CancellationToken cancellationToken)
    {
        var account = await _accountRepository.GetByIdAsync(request.AccountId);
        if (account == null)
            throw new NotFoundException($"No account found with this Id: {request.AccountId}");

        var operations = await _operationtRepository.ListAsync(operation => operation.AccountId == request.AccountId);

        return operations.Select(operation => HistoryDto.FromOperation(operation));
    }
}
