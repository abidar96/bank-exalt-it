﻿using Bank.Application.Application.Exceptions;
using Bank.Domain.Exceptions;
using FluentValidation;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Text.Json;

namespace Bank.Application.Infrastructure.Errors;

public class CustomErrorHandlerHelper
{
    internal static Task WriteDevelopmentResponse(HttpContext httpContext, Func<Task> next)
    {
        _ = next;

        return WriteResponse(httpContext, includeDetails: true);
    }

    internal static Task WriteProductionResponse(HttpContext httpContext, Func<Task> next)
    {
        _ = next;

        return WriteResponse(httpContext, includeDetails: false);
    }

    private static async Task WriteResponse(HttpContext httpContext, bool includeDetails)
    {
        IExceptionHandlerFeature exceptionDetails = httpContext.Features.Get<IExceptionHandlerFeature>();
        Exception exception = exceptionDetails?.Error;

        if (exception != null)
        {
            httpContext.Response.ContentType = "application/problem+json";

            string title = "The server encountered an internal error. Please try the request again.";
            string details = includeDetails ? exception.ToString() : null;
            int status;

            var problem = new ProblemDetails
            {
                Detail = details
            };

            if (exception is FunctionalException)
            {
                status = StatusCodes.Status400BadRequest;
                title = exception.Message;
            }
            else if (exception is ValidationException validationException)
            {
                status = StatusCodes.Status400BadRequest;
                title = "The input model is invalid. See the 'validations' table.";
                problem.Extensions["validations"] = validationException.Errors.Select(error => error.ErrorMessage);
            }
            else if (exception is NotFoundException notFoundException)
            {
                status = StatusCodes.Status404NotFound;
                title = exception.Message;
            }
            else
            {
                status = StatusCodes.Status500InternalServerError;
            }

            problem.Title = title;

            string traceId = Activity.Current?.Id ?? httpContext?.TraceIdentifier;
            if (traceId != null)
                problem.Extensions["traceId"] = traceId;

            httpContext.Response.StatusCode = status;

            Stream stream = httpContext.Response.Body;
            await JsonSerializer.SerializeAsync(stream, problem).ConfigureAwait(false);
        }
    }
}
