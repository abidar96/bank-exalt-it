﻿namespace Bank.Application.Infrastructure.Errors;

public static class ApplicationBuilderExtensions
{
    public static void UseErrors(this IApplicationBuilder application, IHostEnvironment environment)
    {
        if (environment.IsDevelopment())
        {
            application.Use(CustomErrorHandlerHelper.WriteDevelopmentResponse);
        }
        else
        {
            application.Use(CustomErrorHandlerHelper.WriteProductionResponse);
        }
    }
}
