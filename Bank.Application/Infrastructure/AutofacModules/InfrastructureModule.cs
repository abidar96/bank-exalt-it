﻿using Autofac;
using Bank.Domain.SeedWork;
using Bank.Infrastructure.Repositories;
using MediatR;
using System.Reflection;

namespace Bank.Application.Infrastructure.AutofacModules;

public class InfrastructureModule : Autofac.Module
{
    public IConfiguration Configuration { get; }

    public InfrastructureModule(IConfiguration configuration)
    {
        Configuration = configuration;
    }
    protected override void Load(ContainerBuilder builder)
    {
        builder
            .RegisterAssemblyTypes(typeof(AccountRepository).GetTypeInfo().Assembly)
            .Where(type => type.IsClosedTypeOf(typeof(IRepository<>)))
            .AsImplementedInterfaces();
    }
}
