﻿using Autofac;
using Bank.Application.Application.Behaviours;
using Bank.Application.Application.Commands;
using FluentValidation;
using MediatR;
using System.Reflection;

namespace Bank.Application.Infrastructure.AutofacModules;

public class MediatorModule : Autofac.Module
{
    protected override void Load(ContainerBuilder builder)
    {
        builder.RegisterAssemblyTypes(typeof(IMediator).GetTypeInfo().Assembly)
            .AsImplementedInterfaces();

        builder.RegisterAssemblyTypes(typeof(DepositCommand).GetTypeInfo().Assembly)
            .AsClosedTypesOf(typeof(IRequestHandler<,>));

        builder
            .RegisterAssemblyTypes(typeof(DepositCommand).GetTypeInfo().Assembly)
            .Where(type => type.IsClosedTypeOf(typeof(IValidator<>)))
            .AsImplementedInterfaces();

        builder.Register<ServiceFactory>(context =>
        {
            IComponentContext componentContext = context.Resolve<IComponentContext>();
            return type => componentContext.TryResolve(type, out object o) ? o : null;
        });

        builder.RegisterGeneric(typeof(ValidatorBehavior<,>)).As(typeof(IPipelineBehavior<,>));
    }
}
