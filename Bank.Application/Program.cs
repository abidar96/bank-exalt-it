using Autofac;
using Autofac.Extensions.DependencyInjection;
using Bank.Application.Infrastructure.AutofacModules;
using Bank.Application.Infrastructure.Errors;
using Bank.Infrastructure;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;
// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddHttpContextAccessor();
builder.Services.AddDbContext<ApplicationDbContext>(options =>
options.UseSqlite(configuration.GetConnectionString("BankConnection")));
builder.Services.AddControllers();
builder.Services.AddSwaggerGen();
builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory());
builder.Host.ConfigureContainer<ContainerBuilder>(builder => {
    builder.RegisterModule(new InfrastructureModule(configuration));
    builder.RegisterModule(module: new MediatorModule());
});


var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

if (app.Environment.IsDevelopment())
{
    app.UseExceptionHandler(application => application.UseErrors(app.Environment));

}

app.UseSwagger();
app.UseSwaggerUI();
app.UseHttpsRedirection();

app.UseStaticFiles();
app.UseRouting();

app.UseEndpoints(endpoints =>
{
    endpoints.MapRazorPages();
    endpoints.MapControllers();
    endpoints.MapFallbackToFile("index.html");
});

using IServiceScope scope = app.Services.CreateScope();
IServiceProvider services = scope.ServiceProvider;

ApplicationDbContext appDbContext = services.GetRequiredService<ApplicationDbContext>();

if (appDbContext.Database.IsRelational())
{
    appDbContext.Database.Migrate();

}

app.Run();
