﻿using Bank.Domain.SeedWork;

namespace Bank.Domain.Entities;

public class Account : Entity
{
    public int Balance { get; set; }

    public IEnumerable<Operation>? Operations { get; set; }
}
