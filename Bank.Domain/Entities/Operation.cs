﻿using Bank.Domain.SeedWork;

namespace Bank.Domain.Entities;

public class Operation : Entity
{
    public int Amount { get; set; }
    public int Balance { get; set; }
    public DateTime Date { get; set; }

    public int AccountId { get; set; }

    public Account? Account { get; set; }
}
