﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Domain.Exceptions;

public class FunctionalException : Exception
{
    public FunctionalException()
    {
    }

    public FunctionalException(string message) : base(message)
    {
    }

    public FunctionalException(string message, Exception innerException) : base(message, innerException)
    {
    }
}
