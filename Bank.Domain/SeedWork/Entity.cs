﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Domain.SeedWork;

public class Entity
{
    public int Id { get; set; }

    public bool IsTransient()
    {
        return Id == default;
    }
}
