﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Domain.SeedWork;

public interface IRepository<TEntity> where TEntity : Entity
{
    Task<TEntity> AddAsync(TEntity entity);

    TEntity Add(TEntity entity);

    Task<int> CountAsync(Expression<Func<TEntity, bool>> expr);

    void Delete(TEntity entity);

    Task<bool> ExistAsync(int id);

    IQueryable<TEntity> GetAll();

    Task<TEntity> GetByIdAsync(int id);

    Task<List<TEntity>> ListAsync();

    Task<List<TEntity>> ListAsync(Expression<Func<TEntity, bool>> expression);

    Task SaveChangesAsync();

    void Update(TEntity entity);
}
