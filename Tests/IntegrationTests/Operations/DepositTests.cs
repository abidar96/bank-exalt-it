using Bank.Application;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Xunit;

namespace IntegrationTests;

public class DepositTests { 
    private readonly HttpClient _client;

    public DepositTests()
    {
        var app = new CustomWebApplicationFactory<Program>();           
        _client = app.CreateClient();
    }


    [Fact]
    public async Task TestDepositShouldReturn200()
    {

        // Arrange
        int accountId = 1;
        
        HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, $"/api/Operation/Deposit");
        requestMessage.Content = JsonContent.Create(new { accountId = accountId, amount = 100 });

        //ACT
        var response = await _client.SendAsync(requestMessage);

        //Assert
        Assert.Equal(System.Net.HttpStatusCode.OK, response.StatusCode);
            
    }

    [Fact]
    public async Task TestDepositWhenInvalidDataShouldReturn404()
    {

        // Arrange
        int accountId = 2;

        HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, $"/api/Operation/Deposit");
        requestMessage.Content = JsonContent.Create(new { accountId = accountId, amount = 100 });

        //ACT
        var response = await _client.SendAsync(requestMessage);

        //Assert
        Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);

    }

    [Fact]
    public async Task TestDepositWhenInvalidInputShouldReturn400()
    {

        // Arrange
        int accountId = 1;

        HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, $"/api/Operation/Deposit");
        requestMessage.Content = JsonContent.Create(new { accountId = accountId });

        //ACT
        var response = await _client.SendAsync(requestMessage);

        //Assert
        Assert.Equal(System.Net.HttpStatusCode.BadRequest, response.StatusCode);

    }
}
