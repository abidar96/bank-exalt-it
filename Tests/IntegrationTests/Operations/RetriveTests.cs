using Bank.Application;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Xunit;

namespace IntegrationTests
{
    public class RetriveTests { 
        private readonly HttpClient _client;

        public RetriveTests()
        {
            var app = new CustomWebApplicationFactory<Program>();           
            _client = app.CreateClient();
        }


        [Fact]
        public async Task TestRetriveShouldReturn200()
        {
            // Arrange
            int accountId = 1;
            
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, $"/api/Operation/Retrive");
            requestMessage.Content = JsonContent.Create(new { accountId = accountId, amount = 100 });

            //ACT
            var response = await _client.SendAsync(requestMessage);

            //Assert
            Assert.Equal(System.Net.HttpStatusCode.OK, response.StatusCode);
        }


        [Fact]
        public async Task TestRetriveWhenNoBalanceShouldReturn400()
        {
            // Arrange
            int accountId = 1;

            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, $"/api/Operation/Retrive");
            requestMessage.Content = JsonContent.Create(new { accountId = accountId, amount = 500 });

            //ACT
            var response = await _client.SendAsync(requestMessage);

            //Assert
            Assert.Equal(System.Net.HttpStatusCode.BadRequest, response.StatusCode);
        }


        [Fact]
        public async Task TestRetriveWhenInvalidDataShouldReturn404()
        {

            // Arrange
            int accountId = 2;

            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, $"/api/Operation/Retrive");
            requestMessage.Content = JsonContent.Create(new { accountId = accountId, amount = 100 });

            //ACT
            var response = await _client.SendAsync(requestMessage);

            //Assert
            Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);

        }

        [Fact]
        public async Task TestRetriveWhenInvalidInputShouldReturn400()
        {

            // Arrange
            int accountId = 1;

            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, $"/api/Operation/Retrive");
            requestMessage.Content = JsonContent.Create(new { accountId = accountId });

            //ACT
            var response = await _client.SendAsync(requestMessage);

            //Assert
            Assert.Equal(System.Net.HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}