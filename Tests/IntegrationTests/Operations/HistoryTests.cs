﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace IntegrationTests.Operations;

public class HistoryTests
{
    private readonly HttpClient _client;

    public HistoryTests()
    {
        var app = new CustomWebApplicationFactory<Program>();
        _client = app.CreateClient();
    }

    [Fact]
    public async Task TestGetHistoryShouldReturn200()
    {
        // Arrange
        int accountId = 1;

        HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, $"/api/Operation/{accountId}/History");

        //ACT
        var response = await _client.SendAsync(requestMessage);

        //Assert
        Assert.Equal(System.Net.HttpStatusCode.OK, response.StatusCode);
    }

    [Fact]
    public async Task TestGetHistoryWhenNoAccountFoundShouldReturn404()
    {
        // Arrange
        int accountId = 2;

        HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, $"/api/Operation/{accountId}/History");

        //ACT
        var response = await _client.SendAsync(requestMessage);

        //Assert
        Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);
    }

}
