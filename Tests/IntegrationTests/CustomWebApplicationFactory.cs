﻿using Bank.Domain.Entities;
using Bank.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace IntegrationTests;

public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class 
{
    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {

        //base.ConfigureWebHost(builder);
        _ = builder.ConfigureServices(services =>
        {

            var descriptor = services.SingleOrDefault(services => services.ServiceType == typeof(DbContextOptions<ApplicationDbContext>));

            _ = services.Remove(descriptor);

            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseInMemoryDatabase("InMemoryDbForTesting");
            });

            var sp = services.BuildServiceProvider();

            using (var scope = sp.CreateScope())
            {
                var scopedServices = scope.ServiceProvider;
                var db = scopedServices.GetRequiredService<ApplicationDbContext>();

                db.Database.EnsureCreated();

                try
                {
                    var account = db.Accounts.FirstOrDefault();
                    if(account == null)
                    {
                        account = new Account()
                        {
                            Id = 1,
                            Balance = 200
                        };
                    }
                    account.Balance = 200;
                    db.Accounts.Update(account);
                    db.SaveChangesAsync();

                }
                catch (Exception ex)
                {
                    Console.WriteLine($"An error occurred seeding the database with test messages. Error: {ex.Message}");
                }
            }

        });
    }
}
