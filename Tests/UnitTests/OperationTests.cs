using Bank.Domain.Entities;
using Bank.Domain.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Bank.Application.Application.Commands;
using Bank.Application.Application.Queries;
using Bank.Application.Application.Exceptions;
using Bank.Domain.Exceptions;

namespace Bank.Tests;

[TestClass]
public class OperationTests
{
    public readonly DepositCommandHandler _depositCommandHandler;
    public readonly RetriveCommandHandler _retriveCommandHandler;
    public readonly HistoryQueryHandler _historyQueryHandler;
    private readonly Mock<IOperationRepository> _operationRepository = new Mock<IOperationRepository>();
    private readonly Mock<IAccountRepository> _accountRepository = new Mock<IAccountRepository>();

    public OperationTests()
    {
        _depositCommandHandler = new DepositCommandHandler(_accountRepository.Object, _operationRepository.Object);
        _retriveCommandHandler = new RetriveCommandHandler(_accountRepository.Object, _operationRepository.Object);
        _historyQueryHandler = new HistoryQueryHandler(_accountRepository.Object, _operationRepository.Object);
    }

    [TestMethod]
    public void DepositCommandeShouldReturnDepositDTO()
    {
        //Arrange
        var commande = new DepositCommand()
        {
            AccountId = 1,
            Amount = 10
        };

        var account = new Account()
        {
            Id = 1,
            Balance = 0
        };

        var operation = new Operation()
        {
            Id = 1,
            AccountId = 1,
            Account = account,
            Amount = 10,
            Balance = account.Balance+ commande.Amount
        };

        _accountRepository.Setup(x => x.GetByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(account));
        _operationRepository.Setup(x => x.AddAsync(It.IsAny<Operation>())).Returns(Task.FromResult(operation));

        //ACT
        var addedDeposit = _depositCommandHandler.Handle(commande, System.Threading.CancellationToken.None).Result;

        //Assert
        Assert.AreEqual(addedDeposit.Amount,commande.Amount);
        Assert.AreEqual(addedDeposit.Id, operation.Id);
        Assert.AreEqual(addedDeposit.Balance, operation.Balance);
    }

    [TestMethod]
    public void DepositCommandeShouldThrowException()
    {
        //Arrange
        var commande = new DepositCommand()
        {
            AccountId = 1,
            Amount = 10
        };

        var operation = new Operation()
        {
            Id = 1,
            AccountId = 1,
            Amount = 10,
            Balance = commande.Amount
        };
        _operationRepository.Setup(x => x.AddAsync(It.IsAny<Operation>())).Returns(Task.FromResult(operation));
        AggregateException expectedExcetpion = null;
        
        //ACT
        try
        {
            var addedDeposit = _depositCommandHandler.Handle(commande, System.Threading.CancellationToken.None).Result;

        }
        catch (AggregateException ex)
        {
            expectedExcetpion = ex;

        }

        //Assert
        Assert.IsNotNull(expectedExcetpion);
        Assert.AreEqual(expectedExcetpion.Message, "One or more errors occurred. (No account found with this Id: 1)");
    }

    [TestMethod]
    public void RetriveCommandeShouldReturnDepositDTO()
    {
        //Arrange
        var commande = new RetrieveCommand()
        {
            AccountId = 1,
            Amount = 10
        };

        var account = new Account()
        {
            Id = 1,
            Balance = 20
        };

        var operation = new Operation()
        {
            Id = 1,
            AccountId = 1,
            Account = account,
            Amount = commande.Amount * (-1),
            Balance = account.Balance - commande.Amount
        };

        _accountRepository.Setup(x => x.GetByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(account));
        _operationRepository.Setup(x => x.AddAsync(It.IsAny<Operation>())).Returns(Task.FromResult(operation));

        //ACT
        var addedRetrive = _retriveCommandHandler.Handle(commande, System.Threading.CancellationToken.None).Result;

        //Assert
        Assert.AreEqual(addedRetrive.Amount * (-1), commande.Amount);
        Assert.AreEqual(addedRetrive.Balance, operation.Balance);
    }

    [TestMethod]
    public void RetriveCommandeShouldThrowNotFoundException()
    {
        //Arrange
        var commande = new RetrieveCommand()
        {
            AccountId = 1,
            Amount = 10
        };

        var operation = new Operation()
        {
            Id = 1,
            AccountId = 1,
            Amount = 10,
            Balance = commande.Amount
        };
        _operationRepository.Setup(x => x.AddAsync(It.IsAny<Operation>())).Returns(Task.FromResult(operation));
        Exception expectedExcetpion = null;

        //ACT
        try
        {
            var addedDeposit = _retriveCommandHandler.Handle(commande, System.Threading.CancellationToken.None).Result;

        }
        catch (AggregateException ex)
        {
            expectedExcetpion = ex.InnerExceptions.FirstOrDefault();

        }

        //Assert
        Assert.AreEqual(expectedExcetpion.GetType(), typeof(NotFoundException));
        Assert.AreEqual(expectedExcetpion.Message, "No account found with this Id: 1");
    }
    
    [TestMethod]
    public void RetriveCommandeShouldThrowFunctionalException()
    {
        //Arrange
        var commande = new RetrieveCommand()
        {
            AccountId = 1,
            Amount = 100
        };

        var account = new Account()
        {
            Id = 1,
            Balance = 20
        };

        var operation = new Operation()
        {
            Id = 1,
            AccountId = 1,
            Amount = 10,
            Balance = commande.Amount
        };

        _accountRepository.Setup(x => x.GetByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(account));
        _operationRepository.Setup(x => x.AddAsync(It.IsAny<Operation>())).Returns(Task.FromResult(operation));
        Exception expectedExcetpion = null;

        //ACT
        try
        {
            var addedDeposit = _retriveCommandHandler.Handle(commande, System.Threading.CancellationToken.None).Result;

        }
        catch (AggregateException ex)
        {
            expectedExcetpion = ex.InnerExceptions.FirstOrDefault();
        }

        //Assert
        Assert.AreEqual(expectedExcetpion.GetType(), typeof(FunctionalException));
        Assert.AreEqual(expectedExcetpion.Message, "Your balance is not sufficient to perform this operation");
    }

    [TestMethod]
    public void HistoryQueryShouldThrowNotFoundException()
    {
        //Arrange
        var commande = new HistoryQuery()
        {
            AccountId = 1,
        };

        Exception expectedExcetpion = null;

        //ACT
        try
        {
            var addedDeposit = _historyQueryHandler.Handle(commande, System.Threading.CancellationToken.None).Result;

        }
        catch (AggregateException ex)
        {
            expectedExcetpion = ex.InnerExceptions.FirstOrDefault();

        }

        //Assert
        Assert.AreEqual(expectedExcetpion.GetType(), typeof(NotFoundException));
        Assert.AreEqual(expectedExcetpion.Message, "No account found with this Id: 1");
    }

    [TestMethod]
    public void HistoryQueryShouldReturnListOfHistoryDTO()
    {
        //Arrange
        var commande = new HistoryQuery()
        {
            AccountId = 1,
        };

        var account = new Account()
        {
            Id = 1,
            Balance = 20
        };
        
        var operation1 = new Operation()
        {
            Account = account,
            AccountId = 1,
            Amount = 10,
            Balance = 20
        };
        var operation2 = new Operation()
        {
            Account = account,
            AccountId = 1,
            Amount = 10,
            Balance = 30
        };

        var operations = new List<Operation>() { operation1, operation2 };

        _accountRepository.Setup(x => x.GetByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(account));
        _operationRepository.Setup(x => x.ListAsync(It.IsAny<Expression<Func<Operation, bool>>>())).Returns(Task.FromResult(operations));

        //ACT
        var historys = _historyQueryHandler.Handle(commande, System.Threading.CancellationToken.None).Result.ToList();
        //Assert
        Assert.AreEqual(historys.Count(),2);
        Assert.AreEqual(historys[0].Amount, operation1.Amount);
        Assert.AreEqual(historys[0].Balance, operation1.Balance);
        Assert.AreEqual(historys[1].Amount, operation2.Amount);
        Assert.AreEqual(historys[1].Balance, operation2.Balance);
    }
}
