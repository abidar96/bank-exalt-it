﻿using Bank.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bank.Infrastructure.Configurations;

public class OperationConfiguration : IEntityTypeConfiguration<Operation>
{
    public void Configure(EntityTypeBuilder<Operation> builder)
    {
        builder.HasOne(operation => operation.Account)
            .WithMany(account => account.Operations)
            .HasForeignKey(operation => operation.AccountId)
            .OnDelete(DeleteBehavior.Cascade);
    }
}
