﻿using Bank.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Bank.Infrastructure;

public class ApplicationDbContext :DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {
        ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        ChangeTracker.LazyLoadingEnabled = false;

    }

    // Entities

    public DbSet<Account> Accounts { get; set; }

    public DbSet<Operation> Operations { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

        modelBuilder.Entity<Account>()
            .HasData(new Account()
            {
                Id = 1,
                Balance = 0
            });

        base.OnModelCreating(modelBuilder);
    }

}
