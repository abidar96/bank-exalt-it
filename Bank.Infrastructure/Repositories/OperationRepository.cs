﻿using Bank.Domain.Entities;
using Bank.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Infrastructure.Repositories;

public class OperationRepository : EfRepository<Operation>, IOperationRepository
{
    public OperationRepository(ApplicationDbContext dbContext) : base(dbContext)
    {
    }
}
